Show_Model = true;   // Show the model in 3D

module insert_cutout(){
    translate([0,0,5]){
        cube([86,2.4,10], center=true);
    }
}

module light_cutout(){
    translate([0,0,-2.5]){
        cube([58,1.6,5], center=true);
    }
}

module led_cutout(){
    translate([2,0,-7.5]){
        cube([81,4.6,5], center=true);
    }
}

module cable_cutout(){
    translate([81/2+2+2,22/2-2.3,-7.5]){
        cube([4,22,5], center=true);
    }
    translate([81/2+2,1.5,-7.5]){
        rotate([0,0,45])
        cube([5,2,5], center=true);
    }
}

if (Show_Model)
  difference(){
      // Base
      cube([86+20+20,39,20], center=true);
      // Insert cutout
      insert_cutout();
      // Cutout for light to shine through
      light_cutout();
      // cutout for the leds
      led_cutout();
      // cutout for cable
      cable_cutout();
  }
else
  // For generating svg of the different cutout layers
  projection()
  // Cutout layer 1
  insert_cutout();

  // Cutout layer 2
  // light_cutout();

  // Cutout layer 3
  // union(){
  //   led_cutout();
  //   cable_cutout();
  // }

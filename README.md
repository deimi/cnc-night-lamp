# Motive plate

## Size

Acrylic glass: 85 x 130 x 2 mm

## Laser engraving

With Snapmaker 2.0 and 10W laser module, you cannot engrave acrylic glass directly as the wavelength of the laser is not absorbed by the material. But with a neat trick, you can engrave it anyway. Just put some chalk marker on the acrylic glass before engraving it. The laser will burn the chalk and the acrylic glass underneath it. Afterwards you can remove the chalk with a wet cloth. [Source](https://forum.snapmaker.com/t/10w-laser-clear-acrylic-engraved-logo-with-light-base/26105)

Use the following settings in Luban with 10W laser module:

* Preset: Dot-filles engraving
* Method: Fill
* Movement mode: Dot
* Fill interval: 0.14 mm
* Jog Speed: 3000 mm/min
* Dwell Time: 5 ms
* Laser Power: 30%

# Stand

Start with a 126x39x20 piece of oak wood. Mark the center on the top and bottom side, as the stand will be milled from both sides.

Use the following settings in Luban with a 1.5mm flat end mill bit:

* Method: Fill
* Work speed: 300 mm/min
* Plunge speed: 300 mm/min
* Step down: 0.5 mm
* Step over: 1.2 mm
* Jog speed: 1500 mm/min
* Jog height: 1 mm
* Stop height: 80 mm

## Insert cutout
**On the top side**, mill the infill of [insert cutout](./insert_cutout.svg) to a **depth of 10 mm**.

## Cable-Led cutout
**On the bottom side**, mill the infill of [cable-led cutout](./cable_led_cutout.svg) to a **depth of 5 mm**.

## Light cutout
**On the bottom side**, mill the infill of [light cutout](./light_cutout.svg) to a **depth of 10.5 mm**.
